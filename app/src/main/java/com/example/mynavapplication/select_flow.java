package com.example.mynavapplication;

import android.app.Notification;
import android.app.PendingIntent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;

public class select_flow extends Fragment {

    Button btnFirstFlow;
    Button btnSecondFlow;
    Button btnDeepLink;

    public select_flow() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_flow, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(view);
        btnFirstFlow = view.findViewById(R.id.buttonFlow1);
        btnSecondFlow = view.findViewById(R.id.buttonFlow2);
        btnDeepLink = view.findViewById(R.id.buttonFlowDeepLink);

        btnFirstFlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.action_select_flow_to_fow1_frag1);
            }
        });

        btnSecondFlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.flow2_frag1);
            }
        });

        btnDeepLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDeepLink();
            }
        });
    }



    private void handleDeepLink(){

        PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity())
                .setGraph(R.navigation.nav_graph1)
                .setDestination(R.id.flow1_frag3)
                .createPendingIntent();

        Notification notification = new Notification.Builder(getActivity())
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("get to flow2/frag2")
                .setContentText("Here is the deep link")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat.from(getActivity()).notify(10, notification);
    }
}
