package com.example.mynavapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.navigation.Navigation;

public class flow1_frag2 extends Fragment {

    Button btnToThirdFrag;
    EditText editStringTest;

    public flow1_frag2() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_flow1_frag2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnToThirdFrag = view.findViewById(R.id.button2);
        editStringTest = view.findViewById(R.id.ContentEdit);

        btnToThirdFrag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bundle bundle = new Bundle();
                bundle.putString("TextString", editStringTest.getText().toString());
                Navigation.findNavController(view).navigate(R.id.action_flow1_frag2_to_flow1_frag3, bundle);
            }
        });
    }
}
